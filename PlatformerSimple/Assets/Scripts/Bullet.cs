﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public Transform tf;
	public float speed;

	// Use this for initialization
	void Start () {
		tf = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}
    //destroys the bullet and the object the bullet hits
	public void OnTriggerEnter2D (Collider2D otherCollider){
		Destroy (otherCollider.gameObject);
		Destroy (gameObject);
	}
    //has the bullet move in the direction it is facing when fired at a set speed.
	public void Move (){
		tf.position += tf.right * speed;
	}
}
