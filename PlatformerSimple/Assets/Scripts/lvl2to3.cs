﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class lvl2to3 : MonoBehaviour {

	public AudioClip soundToPlay;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
        // Pickup
        AudioSource.PlayClipAtPoint(soundToPlay, GetComponent<Transform>().position, 1.0f);
        // Add to score
        Destroy(gameObject);

        // send player to next level
        SceneManager.LoadScene(3);
    }

}
